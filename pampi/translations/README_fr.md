# PAMPI
## <small>Présentations Avec Markdown, Pandoc, Impress.</small>

----

* **Site Web :** http://pascal.peter.free.fr/pampi.html
* **Email :** pascal.peter at free.fr
* **Licence :** GNU General Public License (version 3)
* **Copyright :** (c) 2017-2021

----

### Les outils utilisés pour développer PAMPI

* [Python](https://www.python.org)
* [PyQt](https://riverbankcomputing.com)
* [marked](https://github.com/markedjs/marked)
* [MarkdownHighlighter](https://github.com/rupeshk/MarkdownHighlighter)

### et ceux utilisés pour les présentations

* [Markdown](https://daringfireball.net/projects/markdown)
* [Pandoc](http://www.pandoc.org)
* [impress.js](https://github.com/impress/impress.js)
* [Bootstrap](https://getbootstrap.com)

