### PDF export configuration

* If your presentation has notes, you can choose whether to print them in the file;
* The procedure create a temporary html file, you can ask to open it in your browser, which will give you more configuration possibilities when printing (page numbering, orientation, etc);
* Finally you can even ask not to create the PDF file.
