#! /usr/bin/python3

import sys, os

if __name__ == "__main__":
    infileName = sys.argv[1]
    with open(infileName) as infile, open("pampi.pro", "w") as outfile:
        # ajout de toutes les sources de plugins
        pluginFiles =  os.scandir("../libs/plugins")
        for p in pluginFiles:
            if p.path.endswith(".py"):
                outfile.write("SOURCES      += " + p.path + "\n")
        outfile.write("\n")
        # ajout du fichier pampi.pro.in
        for l in infile.readlines():
            outfile.write(l)
    
