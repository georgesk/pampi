<!--
HEADER FOR PAMPI CONFIG
TITLE:ANIMATIONS
-->



# {.step data-y=-1000}

## EFFETS D'ANIMATIONS

* quelques effets tout prêts sont disponibles pour animer les étapes de vos présentations
* il suffit de les appeler dans des balises `div` ou `span` (voir les exemples qui suivent)
* vous pouvez en ajouter d'autres en modifiant le fichier css













# {.step data-x=-2000 data-y=0}

<div class="anim-rotate-x">
#### anim-rotate-x

![](data/pampi-help/splash.png)
</div>




# {.step data-x=-1000}

<div class="anim-rotate-x">
![](data/pampi-help/splash.png)
</div>
Ici seule une partie du step est dans le div



# {.step data-x=0}

<div class="anim-rotate-y">
#### anim-rotate-y

![](data/pampi-help/splash.png)
</div>




# {.step data-x=1000}

<div class="anim-rotate-z">
#### anim-rotate-z

![](data/pampi-help/splash.png)
</div>



# {.step data-x=2000}

<div class="anim-scale">
#### anim-scale

![](data/pampi-help/splash.png)
</div>









# {.step data-y=1000 data-x=-2000}

<div class="anim-rotate-y-infinite">
#### anim-rotate-y-infinite

![](data/pampi-help/splash.png)
</div>








# {.step data-y=2000 data-x=-2000}

#### En utilisant la balise span

* blablabla <span class="anim-rotate-x">**anim-rotate-x**</span> blablabla
* blablabla <span class="anim-rotate-y">**anim-rotate-y**</span> blablabla
* blablabla <span class="anim-rotate-z">**anim-rotate-z**</span> blablabla
* blablabla <span class="anim-scale">**anim-scale**</span> blablabla
* blablabla <span class="anim-rotate-y-infinite">**anim-rotate-y-infinite**</span> blablabla









# {#overview .step data-x=0 data-y=0 data-scale=6}
