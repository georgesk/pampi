<!--
HEADER FOR PAMPI CONFIG
TITLE:PAMPI
OTHER TOOLS:|KaTeX
-->



# {.step data-x=-5000 data-y=-3000 data-rotate=-60 data-scale=6}

##  ![](data/pampi-help/pampi_icon.png) PAMPI

Présentations Avec Markdown, Pandoc, Impress.

<!--
PREMIER STEP (ÉTAPE) DE LA PRÉSENTATION.
Les 3 lignes ci-dessus décrivent cette première étape.

La première ligne :
    # {.step ...}
indique que c'est une nouvelle étape.
Chaque étape a cette forme.
On y a indiqué les coordonnées (-5000, -3000), une rotation de -60° et un zoom à 6.

La deuxième ligne commence par demander un titre de niveau 2 (d'où les 2 # au début) et y met une image (pampi_icon.png) suivie du texte PAMPI.
La syntaxe pour une image est :
    ![](emplacement relatif et nom du fichier)

Enfin le texte "Présentations Avec Markdown, Pandoc, Impress." est affiché en texte normal.
-->






<!--
Dans la deuxième étape ci-dessous, il y a un titre de niveau 2, une liste à puces avec 2 niveaux et une partie du texte est en gras (donc entouré par 2 *) :
-->

# {.step data-x=-3000 data-y=-4000 data-scale=0.5}

## Fonctionnement

* **les présentations sont écrites dans des fichiers textes**
    * et sont donc facilement modifiables
    * la syntaxe des fichiers (Markdown) est assez simple
* **elles sont converties en pages web**
    * on les affiche dans son navigateur
    * on peut les mettre en ligne, les emporter sur une clé usb, etc






<!--
Ici il y a 3 liens vers des sites externes.
La syntaxe pour un lien est :
    [texte affiché](adresse du lien)
-->

# {.step data-x=-3000 data-y=-3500 data-scale=0.5}

## Les outils

* **[Markdown](http://daringfireball.net/projects/markdown)** : permet d'écrire le contenu des présentations avec une syntaxe simple
* **[Pandoc](http://www.pandoc.org)** : transforme le fichier Markdown en fichier html
* **[impress.js](https://github.com/impress/impress.js)** : programme javascript permettant à la présentation de fonctionner dans le navigateur (affichage des étapes, transitions, gestion du clavier et de la souris, ...)





# {.step data-x=-3000 data-y=-3000 data-scale=0.5}

## L'interface

Le fichier Markdown est affiché à gauche et on peut visualiser le résultat à droite

![](data/pampi-help/pampi-help-01.jpeg)

















# {.step data-x=0 data-y=-3000}

## Les étapes

Chaque étape (step) d'une présentation est positionnée \
où l'on veut dans un espace en 3D

* on indique ses coordonnées
* on peut aussi lui donner un facteur de zoom
* et faire des rotations









<!--
Un truc :
le fichier html généré par Pandoc ne mettra jamais plus d'un saut de ligne entre 2 paragraphes.
Du coup pour arriver à espacer verticalement 2 parties de l'étape, j'ai mis un caractère non affichable (<br/>).
Du coup il y a une ligne sans texte visible et le tour est joué.
-->

# {.step data-x=1000 data-y=-3000}

## Exemples

Cette étape est placée aux coordonnées (1000, -3000, 0). \
La ligne qui la déclare est donc :

#### `# {.step data-x=1000 data-y=-3000}`

<br/>

* `# {.step}` indique que c'est une étape
* `data-x=2000` et `data-y=-3000` donnent les coordonnées
* pas besoin d'indiquer `data-z` puisqu'il vaut 0










# {.step data-x=1000 data-y=-3000 data-z=-1500}

Ici on a ajouté `data-z=-1500`







# {.step data-x=0 data-y=-2000 data-scale=6}

### `data-scale=6`






# {.step data-x=0 data-y=-2500 data-scale=0.2}

### `data-scale=0.2`





# {.step data-x=1700 data-y=-3000 data-rotate=90}

### `data-rotate=90`





# {.step data-x=2000 data-y=-3000 data-rotate=360}

### `data-rotate=360`





# {.step data-x=2000 data-y=-3300 data-rotate=360 data-rotate-x=60}

### `data-rotate-x=60`





# {.step data-x=2500 data-y=-3300 data-rotate=360 data-rotate-x=60 data-rotate-y=60}

### `data-rotate-y=60`










# {.step .text-right data-x=3000 data-y=-3000}

### Alignement

Par défaut, les étapes sont centrées sur l'écran.

Ici on a ajouté `.text-right` \
(avec un point au début).

L'étape est ainsi alignée à droite.









# {.step .slide data-x=4000 data-y=-3000}

L'attribut `.slide` crée un effet de diapositive (fond blanc).








# {.step data-x=5000 data-y=-3000}

### Liste des directives 

* **coordonnées** : data-x, data-y, data-z
* **zoom** : data-scale
* **rotations** : data-rotate, data-rotate-x, data-rotate-y
* **alignement** : .text-left, .text-right
* **effet diapositive** : .slide
































# {.step data-x=-3000 data-scale=4}

## MARKDOWN

Quelques exemples


















# {.step data-x=-5000 data-y=2000}

## Titre de niveau 2
### Titre de niveau 3
#### Titre de niveau 4
##### Titre de niveau 5
###### Titre de niveau 6

----

texte normal

en **gras**, en *italique* ou ~~barré~~





# {.step data-x=-3500 data-y=2000}

* une liste à puces
* la suite
    * une entrée inférieure
    * suite
* on revient au premier niveau






# {.step data-x=-2000 data-y=2000}

1. une liste numérotée
#. la suite
    i. une entrée inférieure
    #. suite
#. on revient au premier niveau











# {.step data-x=-500 data-y=2000}

## On peut écrire des maths

#### en utilisant [KaTeX](https://khan.github.io/KaTeX)

$a^2 + b^2 = c^2$

$v(t) = v_0 + \frac{1}{2}at^2$

$\int_{0}^{1} x dx = \left[ \frac{1}{2}x^2 \right]_{0}^{1} = \frac{1}{2}$

$e^x = \sum_{n=0}^\infty \frac{x^n}{n!} = \lim_{n\rightarrow\infty} (1+x/n)^n$







# {.step data-x=1000 data-y=2000}

### Des liens

* un lien vers un site : [Wikipédia](https://fr.wikipedia.org)
* un lien vers un fichier
    * [un fichier PDF](data/pampi-help/tableau_conversion_volumes.pdf)
    * [un fichier GeoGebra](data/pampi-help/n4a.ggb)

#### Remarque

les fichiers sont placés dans un sous-dossier de **data**  
(le plus simple est de faire un dossier par présentation).





# {.step data-x=2500 data-y=2000}

## Une image

![](data/pampi-help/splash.png)





# {.step data-x=4000 data-y=2000}

## Une vidéo

<div class="figure">
<video controls allowfullscreen class="embed-responsive-item">
<source src="data/pampi-help/tour.mp4" type="video/mp4">
</video>
</div>






# {.step data-x=5000 data-y=2000}


#### Ouvrez le fichier **pampi-help** 
#### pour comparer son contenu 
#### avec cette présentation.

















<!--
NE SUPPRIMEZ PAS CE DERNIER STEP.
Il sert à afficher la vue globale.
Par contre vous pouvez modifier la valeur de "data-scale".
-->
# {#overview .step data-x=0 data-y=0 data-scale=14}
