
## VOUS POUVEZ COPIER-COLLER ET ADAPTER LES EXEMPLES CI-DESSOUS  
## DANS VOS FICHIERS DE PRÉSENTATIONS

### Vous pouvez même rendre ce fichier éditable 
### pour y insérer vos propres exemples.






*********************************************
*********************************************
## EXEMPLES D'ÉTAPES
*********************************************
*********************************************

# {.step data-x=1000 data-y=1000 data-rotate=60 data-scale=2}

# {.step data-x=1000 data-y=1000}

# {.step .text-left data-x=1000 data-y=1000}


### Liste des directives 

* **coordonnées** : data-x, data-y, data-z
* **zoom** : data-scale
* **rotations** : data-rotate, data-rotate-x, data-rotate-y
* **alignement** : .text-left, .text-right
* **effet diapositive** : .slide







*********************************************
*********************************************
## EXEMPLES DE FORMATAGE MARKDOWN
*********************************************
*********************************************

## Titre de niveau 2
### Titre de niveau 3
#### Titre de niveau 4
##### Titre de niveau 5
###### Titre de niveau 6

----

texte normal

en **gras**, en *italique* ou ~~barré~~


### Listes

* une liste à puces
* la suite
    * une entrée inférieure
    * suite
* on revient au premier niveau


1. une liste numérotée
#. la suite
    i. une entrée inférieure
    #. suite
#. on revient au premier niveau


### Des liens

[Wikipédia](https://fr.wikipedia.org)

[un fichier PDF](data/pampi-help/tableau_conversion_volumes.pdf)

[un fichier GeoGebra](data/pampi-help/n4a.ggb)


### Une image

![](data/pampi-help/splash.png)



### Beaucoup d'exemples sont disponibles à la page suivante

http://enacit1.epfl.ch/markdown-pandoc

### Pour écrire des formules mathématiques

https://en.wikibooks.org/wiki/LaTeX/Mathematics







*********************************************
*********************************************
## QUELQUES CARACTÈRES POUVANT ÊTRE UTILES
*********************************************
*********************************************

### pour insérer une ligne vide

<br/>



### autres caractères utiles

√ + - × ÷ ± ≠ ↦ ⟼ ∈ ∉ ≈ → ≤ ⊥ π ∞ 

½ « » ’ … ‰ € – — ‐ ⸮ ¹ ² ³ ⁴ ⁵ ⁶ ⁷ ⁸ ⁹ ⁰






*********************************************
*********************************************
## POUR ÉCRIRE DES NOTES 
## VISIBLES DANS LA CONSOLE
*********************************************
*********************************************

<div class="notes">
Texte de la note (avec formatage éventuel)
</div>



