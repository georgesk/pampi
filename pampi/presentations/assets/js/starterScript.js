var lecteur;
/**
 * la fonction timecode permet de placer le curseur de temps
 * d'un élément audio ou video à un instant donné
 *
 * @param id l'identifiant de l'élément audio ou vidéo 
 *
 * @param secondes le code temporel où il faut démarrer ; si
 *        ce code est négatif, ça revient au début et ça met
 *        en pause.
 **/
function timecode(id, secondes) {
    lecteur = document.getElementById(id);
    if (secondes > 0) {
        lecteur.currentTime = secondes;
        lecteur.play();
    } else {
        lecteur.currentTime = 0;
        lecteur.pause();
    }
}
