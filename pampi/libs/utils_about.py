# -*- coding: utf-8 -*-

# -----------------------------------------------------------------
# This file is a part of PAMPI project.
# Name:         PAMPI
# Copyright:    (C) 2017-2024 Pascal PETER
# Licence:      GNU General Public Licence version 3
# Website:      http://pascal.peter.free.fr/
# Email:        pascal.peter at free.fr
# -----------------------------------------------------------------
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# -----------------------------------------------------------------

"""
DESCRIPTION :
    La fenêtre "À propos"
"""

# importation des modules utiles :
import utils, utils_functions, utils_webengine, utils_filesdirs

from PyQt5 import QtCore, QtWidgets, QtGui



"""
****************************************************
    LA FENÊTRE À PROPOS
****************************************************
"""

class AboutDlg(QtWidgets.QDialog):
    """
    explications
    """
    def __init__(self, parent=None, locale='', icon='./images/icon.png'):
        super(AboutDlg, self).__init__(parent)
        self.main = parent

        # En-tête de la fenêtre :
        size = utils.STYLE['PM_LargeIconSize'] * 4
        logoLabel = QtWidgets.QLabel()
        logoLabel.setMaximumSize(size, size)
        logoLabel.setAlignment(QtCore.Qt.AlignHCenter | QtCore.Qt.AlignVCenter)
        iconPixmap = utils.doIcon('icon', what='PIXMAP')
        logoLabel.setPixmap(iconPixmap)
        translatedText0 = QtWidgets.QApplication.translate(
            'main', 'About {0}')
        translatedText0 = utils_functions.u(
            translatedText0).format(utils.PROGTITLE)
        translatedText1 = QtWidgets.QApplication.translate(
            'main', '(version {0})')
        translatedText1 = utils_functions.u(
            translatedText1).format(utils.PROGVERSION)
        translatedText = utils_functions.u(
            '<h1>{0}</h1>'
            '<h4>{1}</h4>').format(
                translatedText0, translatedText1)
        titleLabel = QtWidgets.QLabel(translatedText)
        titleGroupBox = QtWidgets.QGroupBox()
        titleLayout = QtWidgets.QHBoxLayout()
        titleLayout.addWidget(logoLabel)
        titleLayout.addWidget(titleLabel)
        titleGroupBox.setLayout(titleLayout)

        # Zone d'affichage :
        tabWidget = QtWidgets.QTabWidget()
        mdFile = utils_functions.doLocale(
            locale, 'translations/README', '.md')
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName=mdFile, fileType='MD'),
            QtWidgets.QApplication.translate('main', 'About'))
        tabWidget.addTab(
            FileViewTab(parent=self.main, fileName='COPYING'),
            QtWidgets.QApplication.translate('main', 'License'))

        # Les boutons :
        closeButton = QtWidgets.QPushButton(
            utils.doIcon('dialog-close'), 
            QtWidgets.QApplication.translate('main', 'Close'))
        closeButton.clicked.connect(self.accept)
        buttonLayout = QtWidgets.QHBoxLayout()
        buttonLayout.addStretch(1)
        buttonLayout.addWidget(closeButton)

        # Mise en place :
        mainLayout = QtWidgets.QVBoxLayout()
        mainLayout.addWidget(titleGroupBox)
        mainLayout.addWidget(tabWidget)
        mainLayout.addLayout(buttonLayout)
        self.setLayout(mainLayout)
        self.setWindowTitle(translatedText0)


class FileViewTab(QtWidgets.QWidget):
    """
    pour afficher un fichier txt, html ou md.
    """
    def __init__(self, parent=None, fileName='', fileType='TXT'):
        super(FileViewTab, self).__init__(parent)
        self.main = parent
        self.waiting = True
        # mise en place du conteneur :
        if fileType in ('HTML', 'MD'):
            widget = utils_webengine.MyWebEngineView(
                self, linksInBrowser=True)
            container = QtWidgets.QAbstractScrollArea()
            vBoxLayout = QtWidgets.QVBoxLayout()
            vBoxLayout.addWidget(widget)
            container.setLayout(vBoxLayout)
        else:
            widget = container = QtWidgets.QTextEdit()
            widget.setReadOnly(True)
        mainLayout = QtWidgets.QHBoxLayout()
        mainLayout.addWidget(container)
        self.setLayout(mainLayout)
        # ouverture du fichier :
        if fileType == 'HTML':
            fileName = utils_functions.u(
                '{0}/{1}').format(self.main.beginDir, fileName)
            url = QtCore.QUrl().fromLocalFile(fileName)
            widget.load(url)
        elif fileType == 'MD':
            outFileName = utils_filesdirs.md2html(self.main, fileName)
            url = QtCore.QUrl().fromLocalFile(outFileName)
            widget.load(url)
        else:
            fileContent = utils_filesdirs.readTextFile(fileName)
            widget.setPlainText(fileContent)
        self.waiting = False


