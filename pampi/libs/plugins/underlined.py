"""
Un plugin destiné à Pampi, pour insérer une animation dans l'éditeur
à la sélection courante du curseur. La classe Plugin concerne une 
"police italique"
"""

from PyQt5 import QtCore


from ._font_template import Plugin as FontPlugin
from .plugin_rc import *

class Plugin(FontPlugin):
    def __init__(self, parent):
        FontPlugin.__init__(
            self, parent,
            iconPath = ':/img/icons/underlined.png',
            title = QtCore.QCoreApplication.translate("main","Underlined"),
            name = QtCore.QCoreApplication.translate("main","Underlined"),
            font_marker = "u",
            html_syntax=True
        )
        return
        
