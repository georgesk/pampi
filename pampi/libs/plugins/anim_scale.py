"""
Un plugin destiné à Pampi, pour insérer une animation dans l'éditeur
à la sélection courante du curseur. La classe Plugin concerne une 
"animation zoom"
"""

try:
    from PyQt5 import QtCore, QtWidgets, QtGui
except:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui


from ._anim_template import Plugin as AnimPlugin, AnimDialog
from .plugin_rc import *

class Plugin(AnimPlugin):
    def __init__(self, parent):
        AnimPlugin.__init__(
            self, parent,
            iconPath = ':/img/icons/scale.png',
            title = QtCore.QCoreApplication.translate("main","Make a scaling animation"),
            name = QtCore.QCoreApplication.translate("main","Scale"),
            animation_type = "anim-scale"
        )
        return
        
