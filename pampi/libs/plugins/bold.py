"""
Un plugin destiné à Pampi, pour insérer une animation dans l'éditeur
à la sélection courante du curseur. La classe Plugin concerne une 
"police grasse"
"""

from PyQt5 import QtCore


from ._font_template import Plugin as FontPlugin
from .plugin_rc import *

class Plugin(FontPlugin):
    def __init__(self, parent):
        FontPlugin.__init__(
            self, parent,
            iconPath = ':/img/icons/bold.png',
            title = QtCore.QCoreApplication.translate("main","Bold"),
            name = QtCore.QCoreApplication.translate("main","Bold"),
            font_marker = "**"
        )
        return
        
