"""
Un plugin destiné à Pampi, pour insérer une animation dans l'éditeur
à la sélection courante du curseur. La classe Plugin concerne une 
"police machine à écrire"
"""

try:
    from PyQt5 import QtCore, QtWidgets, QtGui
except:
    from PyQt4 import QtCore, QtGui as QtWidgets, QtGui


from ._font_template import Plugin as FontPlugin
from .plugin_rc import *

class Plugin(FontPlugin):
    def __init__(self, parent):
        FontPlugin.__init__(
            self, parent,
            iconPath = ':/img/icons/tt.png',
            title = QtCore.QCoreApplication.translate("main","Typewriter text"),
            name = QtCore.QCoreApplication.translate("main","Typewriter text"),
            font_marker = "`"
        )
        return
        
