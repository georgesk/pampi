"""
Un plugin destiné à Pampi, pour insérer une animation dans l'éditeur
à la sélection courante du curseur. La classe Plugin concerne une 
"police abstraite", il faut la spécialiser
"""

from PyQt5 import QtCore

import os, re

from ._template import Plugin as Template

font_template = """{marker}{content}{marker}"""
font_template1 = """<{marker}>{content}</{marker}>"""

font_pattern = re.compile(r"[\s\n]*(?P<marker>[\*_`]+)(?P<content>.+)(?P=marker)", re.M|re.S)
font_pattern1 = re.compile(r"[\s\n]*<(?P<marker>[^>]+)>(?P<content>.+)</(?P=marker)>", re.M|re.S)


class Plugin(Template):
    """
    Le constructeur prend comme paramètre la fenêtre principale
    de l'application. Il doit définir les propriété name et action
    @param mainWindow la fenêtre pincipale
    """
    def __init__(
            self, mainWindow,
            font_marker = "??",
            html_syntax = False,
            iconPath = '/usr/share/icons/gnome/48x48/emotes/face-cool.png',
            title = None,
            name = "font-undefined",
    ):
        super(Plugin, self).__init__(
            mainWindow,
            iconPath = iconPath,
            name=name,
            title = title
        )
        self.font_marker = font_marker
        self.html_syntax = html_syntax
        return
    
    def _run(self):
        """
        Fonction de rappel pour le plugin
        """
        fragment = self.getSelected()
        # on essaie la syntaxe HTML
        m = font_pattern1.search(fragment)
        # puis on essaie une syntaxe Markdown
        if not m:
            m = font_pattern.search(fragment)
        # si ça matche, les groupes suivants sont définis :
        # "marker", "content"
        if m:
            marker = m.group("marker")
            content = m.group("content")
        # si ça ne matche toujours pas, on met le fragment
        else:
            marker = "??"
            content = fragment.replace("\n", " ").strip()
            if not content:
                content =  QtCore.QCoreApplication.translate("main","REPLACE ME")
        if self.html_syntax:
            self.insertSelected(font_template1.format(
                content = content,
                marker = self.font_marker
            ))
        else:
            self.insertSelected(font_template.format(
                content = content,
                marker = self.font_marker
            ))
        return
        
    
